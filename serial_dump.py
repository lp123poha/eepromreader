
# Serial1.py

from serial import Serial
import time

port = "/dev/ttyACM0"  # Raspberry Pi 2
#port = "/dev/ttyS0"    # Raspberry Pi 3

data = [None]*(0x800)

def readLine(port):
    s = ""
    while True:
        ch = port.read()
        s += ch
        if ch == '\r':
            return s

def dumpBank():
    ser.write(b'dump')
    return readBank()

def readBank():
    data = []
    for i in range(0x800):
        if(i % 100):
            print(i, end='\r')
        data.append(ser.read(1))
    return data

def writeData(data):
    newFile = open("eeprom_dump.bin", "wb")
    # write to file
    for i in data:
        newFile.write(i)

def hexdump(data):
    for i in range(len(data)):
        if (i%16 == 0):
            print(" ")
            print("0x{:04x}: ".format(i), end='')
        print("0x{:02x}, ".format(data[i]), end='')

ser = Serial(port, baudrate = 9600)
time.sleep(2)
ser.isOpen()
print ("starting")
#ser.write(0x30)
#print(ser.readline())
data = dumpBank()
writeData(data)
#hexdump(data)





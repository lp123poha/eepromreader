// --------------------------------------
// i2c_scanner
//
// Version 1
//    This program (or code that looks like it)
//    can be found in many places.
//    For example on the Arduino.cc forum.
//    The original author is not know.
// Version 2, Juni 2012, Using Arduino 1.0.1
//     Adapted to be as simple as possible by Arduino.cc user Krodal
// Version 3, Feb 26  2013
//    V3 by louarnold
// Version 4, March 3, 2013, Using Arduino 1.0.3
//    by Arduino.cc user Krodal.
//    Changes by louarnold removed.
//    Scanning addresses changed from 0...127 to 1...119,
//    according to the i2c scanner by Nick Gammon
//    https://www.gammon.com.au/forum/?id=10896
// Version 5, March 28, 2013
//    As version 4, but address scans now to 127.
//    A sensor seems to use address 120.
// Version 6, November 27, 2015.
//    Added waiting for the Leonardo serial communication.
//
//
// This sketch tests the standard 7-bit addresses
// Devices with higher bit address might not be seen properly.
//


uint8_t block_buffer[0xff];

void setup() {

  Serial.begin(9600);
  while (!Serial); // Leonardo: wait for serial monitor
  //Serial.println("\nEEPROM Reader");
  TWIInit();
  digitalWrite(A4, 1);
  digitalWrite(A5, 1);
}

void loop() {
  get_cmd();
}

void get_cmd(){
  String cmd = Serial.readString();
  if(cmd.equals("dump")){
    for(int i = 0; i < 8; i++){
      int error = i2c_read_block(i);
      if(error){
        //Serial.println(error, HEX);
      }else{
        dumpBuffer();
      }
    }
  }
}


void dumpBuffer(){
  for(uint8_t i = 0; i< 0x100; i++){
    if(i%16==0){
      //Serial.print("\n");
    }
    Serial.write(block_buffer[i]);
  }
}

void TWIInit(void)
{
    //set SCL to 400kHz
    TWSR = 0x00;
    TWBR = 0x48;
    //enable TWI
    TWCR = (1<<TWEN);
}


int i2c_read_block(uint8_t block_addr){
  uint8_t data = 0;
  if(!i2c_start()){
    return 1;
  }
  TWDR = ((0x50 | block_addr) << 1); // device addr + WR Bit in data buffer
  TWCR = (1<<TWINT) |(1<<TWEN); // start transmission
  while (!(TWCR & (1<<TWINT))); // wait until transmission completed
  if ((TWSR & 0xF8) != 0x18){
    i2c_stop();
    return 2;
  }
  TWDR = 0x00;
  TWCR = (1<<TWINT) | (1<<TWEN);
  while (!(TWCR & (1<<TWINT)));
  if ((TWSR & 0xF8)!= 0x28){
    i2c_stop();
    return 3;
  }
  if(!i2c_repeated_start()){
    i2c_stop();
    return 4;
  }
  TWDR = ((0x50 | block_addr) << 1) | 0x01; // device addr + WR Bit in data buffer
  TWCR = (1<<TWINT) |(1<<TWEN); // start transmission
  while (!(TWCR & (1<<TWINT))); // wait until transmission completed
  if ((TWSR & 0xF8) != 0x40){
    i2c_stop();
    return 5;
  }
  for(uint8_t i = 0; i < 0xff; i++){
    TWCR = (1<<TWINT) |(1<<TWEN) |(1<<TWEA); // start transmission
    while (!(TWCR & (1<<TWINT)));
    if ((TWSR & 0xF8) != 0x50){
      i2c_stop();
      return 6;
    }
    else{
      block_buffer[i] = TWDR;
    }
  }
  i2c_stop();
  return 0;
} 

void i2c_stop(){
  TWCR = (1<<TWINT)|(1<<TWSTO)|(1<<TWEN);
}

bool i2c_start(){
  TWCR = (1<<TWINT)|(1<<TWSTA)|(1<<TWEN);
  while (!(TWCR & (1<<TWINT)));
  if ((TWSR & 0xF8) != 0x08){
    return false;
  }
  return true;
}

bool i2c_repeated_start(){
  TWCR = (1<<TWINT)|(1<<TWSTA)|(1<<TWEN);
  while (!(TWCR & (1<<TWINT)));
  if ((TWSR & 0xF8) != 0x10){
    return false;
  }
  return true;
}
